/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *Tutoring with Rudolph Tejada at CCNY Steiman 2M-14 (Frennie & Tubaa)
 *Satya Prakash (Meghna)
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:13 hours
 */ 

#include <iostream>
using namespace std;
#include <string>
#include <set>
#include <vector>

// write this function to help you out with the computation.
//unsigned long countWords(const string& s, set<string>& wl);

int main()
{
 vector<string> wordList;
 string line;
 string temp = "";
 vector<string> lines;
 int numline = 0;
 int numchar=0;
 while (getline(cin,line))
 {
  ++numline;
  line = line + ' ';
  numchar = numchar+line.length();
  lines.push_back(line);

  for(int i=0; i<line.length(); i++)
  {
   if(isalpha(line[i])){
   temp = temp + line[i];
   }
    else{
      wordList.push_back(temp);
      temp = "";
    }
  }
}
 
 for (int i=0; i<wordList.size(); i++){
   if(wordList[i].size()==0)
   {
     wordList.erase(wordList.begin()+i);
     i--;
   }
 }
 int uw = wordList.size();
 int ul=lines.size();
 for (int i=0; i<wordList.size(); i++)
 {
 for (int j=i+1; j<wordList.size(); j++)
 { 
 if (wordList[i]==wordList[j])
 { 
 uw--;
 break;
 }
 }
 }
for (int i=0; i<lines.size(); i++)
 {
  for(int j=i+1; j<lines.size(); j++)
  {
   if (lines[i]==lines[j])
   {
    ul--;
   }
  }
 }
 cout << numline<<"\t"<< wordList.size() <<"\t"<<numchar<<"\t"<< ul << "\t" << uw << "\t";
 return 0;
}

